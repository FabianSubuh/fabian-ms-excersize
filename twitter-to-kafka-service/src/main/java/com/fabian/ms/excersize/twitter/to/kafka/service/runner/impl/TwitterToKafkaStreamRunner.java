package com.fabian.ms.excersize.twitter.to.kafka.service.runner.impl;

import com.fabian.ms.excersize.config.TwitterToKafkaConfigData;
import com.fabian.ms.excersize.twitter.to.kafka.service.listener.TwitterToKafkaStatusListener;
import com.fabian.ms.excersize.twitter.to.kafka.service.runner.StreamRunner;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import twitter4j.FilterQuery;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

import java.util.Arrays;

@Component
@ConditionalOnProperty(name = "twitter-to-kafka-service.enable-mock-tweets", havingValue = "false")
public class TwitterToKafkaStreamRunner implements StreamRunner {
    private final TwitterToKafkaConfigData configData;

    private final TwitterToKafkaStatusListener listener;

    private TwitterStream twitterStream;

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(TwitterToKafkaStreamRunner.class);

    public TwitterToKafkaStreamRunner(TwitterToKafkaConfigData configData, TwitterToKafkaStatusListener listener) {
        this.configData = configData;
        this.listener = listener;
    }


    @Override
    public void start() throws TwitterException {
        twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(listener);
        addFilter();
    }

    @PreDestroy
    public void shutdown(){
        if (twitterStream != null) {
            LOG.info("Closing twitter stream!");
            twitterStream.shutdown();
        }
    }

    private void addFilter() {
        String[] keywords = configData.getTwitterKeywords().toArray(new String[0]);
        FilterQuery filterQuery = new FilterQuery(keywords);
        twitterStream.filter(filterQuery);
        LOG.info("Started filtering data from keywords", Arrays.toString(keywords));
    }
}
