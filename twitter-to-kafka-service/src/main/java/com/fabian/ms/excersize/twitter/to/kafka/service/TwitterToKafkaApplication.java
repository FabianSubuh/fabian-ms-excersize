package com.fabian.ms.excersize.twitter.to.kafka.service;

import com.fabian.ms.excersize.config.TwitterToKafkaConfigData;
import com.fabian.ms.excersize.twitter.to.kafka.service.init.StreamInitializer;
import com.fabian.ms.excersize.twitter.to.kafka.service.runner.StreamRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;

@SpringBootApplication
@ComponentScan(basePackages = "com.fabian.ms.excersize")
public class TwitterToKafkaApplication implements CommandLineRunner {

    private final TwitterToKafkaConfigData configData;

    private static final Logger LOG = LoggerFactory.getLogger(TwitterToKafkaApplication.class);

    private final StreamRunner streamRunner;

    private final StreamInitializer streamInitializer;
    public static void main(String[] args) {
        SpringApplication.run(TwitterToKafkaApplication.class,args);
    }

    public TwitterToKafkaApplication(TwitterToKafkaConfigData configData, StreamRunner streamRunner, StreamInitializer streamInitializer) {
        this.configData = configData;
        this.streamRunner = streamRunner;
        this.streamInitializer = streamInitializer;
    }

    @Override
    public void run(String... args) throws Exception {
        LOG.info("App Start ...");
        LOG.info(Arrays.toString(configData.getTwitterKeywords().toArray(new String[] {})));
        streamInitializer.init();
        streamRunner.start();
    }
}
