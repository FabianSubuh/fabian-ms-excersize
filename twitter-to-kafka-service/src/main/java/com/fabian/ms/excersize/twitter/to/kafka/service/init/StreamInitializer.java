package com.fabian.ms.excersize.twitter.to.kafka.service.init;

public interface StreamInitializer {
    void init();
}
